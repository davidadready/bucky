# Ruby 2.2.2

require 'sinatra'
require 'aws-sdk-v1'
require 'json'
require 'pry' if Sinatra::Base.development?

class BuckyApp < Sinatra::Base
  get '/' do
    erb :greeting
  end

  get '/:version' do
    erb :greeting
  end

  post '/' do
    status 401
    content_type :json
    {:error => "Must pass version (e.g. \"v1\")"}.to_json
  end

  post '/:version' do
    unless params['version'] == 'v1'
      status 401
      content_type :json
      return {:error => "Bad version: #{params['version']}"}.to_json
    end

    file_upload = params['fileUpload'] || params['file_upload']
    is_authorized, auth_failure_reason = authorize(params['token'])

    if is_authorized
      unless file_upload
        status 418
        content_type :json
        return {:error => 'No file'}.to_json
      end
                                           # expected:
      type     = file_upload[:type]        # "application/json"
      filename = file_upload[:filename]    # "123-123.swf.json"
      file     = file_upload[:tempfile]    # a json file

      success, message = S3Bucket.upload(file, filename)

      if success
        status 201
        content_type :json
        {:message => message}.to_json
      else
        status 418
        content_type :json
        {:error => message}.to_json
      end
    else
      status 401
      content_type :json
      {:error => auth_failure_reason}.to_json
    end
  end

  private
  def authorize(token)
    return [false, 'Auth uninitialized on server'] unless BUCKY_SECRET
    return [false, 'No token'] unless token
    payload = decode_token(token)
    return [false, 'Cannot decode token'] unless payload
    iat = payload[0]['iat']
    return [false, 'Bad token'] unless iat
    return [false, 'Token Expired'] if expired?(iat)

    [true, nil]
  end

  def expired?(iat)
    one_minute = 60
    Time.at(iat) > Time.now + (one_minute * 30)
  end

  def decode_token(token)
    begin
      JWT.decode(token, BUCKY_SECRET, true, :algorithm => 'HS256')
    rescue
      nil
    end
  end
end
