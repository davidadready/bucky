class S3Bucket
  class << self
    def upload(file_data, path)
      key = "#{S3Settings.swiffy_dir}/#{path}"

      begin
        swiffy_bucket.objects[key].write(:data => file_data) # or :io or :string

        [true, "File uploaded to http://#{S3Settings.swiffy_bucket_uri}/#{path}"]
      rescue Errno::ENOENT => e
        [false, e]
      end
    end

    private
    def swiffy_bucket
      @swiffy_bucket ||= s3_connection.buckets[S3Settings.swiffy_bucket]
    end

    def s3_connection
      @s3_connection ||= begin
        raise 'Must have S3_ACCESS_ID and S3_SECRET constants defined.' unless S3_ACCESS_ID && S3_SECRET
        AWS.config(:access_key_id => S3_ACCESS_ID, :secret_access_key => S3_SECRET)
        AWS::S3.new
      end
    end
  end
end
