**Bucky**  
A microservice for communicating with our S3 buckets. Built on Sinatra v1.4.6 on Ruby 2.2.2.

To run this microservice, you will need to add a **keys.rb** file to the root directory that defines the S3 access ID and secret,
as well as a "BUCKY_SECRET" that matches the main app for authentication purposes
It should look a little something like this:


```
#!ruby
S3_ACCESS_ID = 'whencaptainamericathrowshismightyshield'
S3_SECRET    = 'allthosewhochosetoopposehisshieldmustyield'
BUCKY_SECRET = 'doesnotactuallyyieldtoshield'
```


To upload a file, POST with these parameters:  
1. fileUpload (or file_upload): contains filedata  
2. token: contains JWT token with an 'iat' value (unix epoch time in seconds of when the token was issued) less than 30 minutes old. Token is encoded in 'HS256' with a shared secret constant named BUCKY_SECRET