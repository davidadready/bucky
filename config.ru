require 'rubygems'
require 'bundler'

Bundler.require

require './app'
require './s3_settings'
require './s3_bucket'

begin
  require './keys'
rescue LoadError
  raise LoadError, "You must add a file named keys.rb to the app's root directory that defines the constants S3_ACCESS_ID and S3_SECRET!"
end

set :server, :thin

run BuckyApp
