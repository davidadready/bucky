class S3Settings
  BUCKET_SETTINGS = {
      :production => {
          :swiffy_dir => 'prod',
          :swiffy_bucket_uri => '48544d4c5.adready.com',
          :swiffy_bucket => 'swfhtml5',
      },
      :development => {
          :swiffy_dir => 'dev', # directory to place json in on S3 bucket
          :swiffy_bucket_uri => 'swfhtml5.s3.amazonaws.com/dev', # URI to access said directory from the tag
          :swiffy_bucket => 'swfhtml5' # bucket name
      }
  }

  class << self
    def swiffy_dir
      BUCKET_SETTINGS[production_or_development?][:swiffy_dir]
    end

    def swiffy_bucket_uri
      BUCKET_SETTINGS[production_or_development?][:swiffy_bucket_uri]
    end

    def swiffy_bucket
      BUCKET_SETTINGS[production_or_development?][:swiffy_bucket]
    end

    private
    def production_or_development?
      BuckyApp.production? ? :production : :development
    end
  end
end